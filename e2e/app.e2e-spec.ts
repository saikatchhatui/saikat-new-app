import { SaiagPage } from './app.po';

describe('saiag App', () => {
  let page: SaiagPage;

  beforeEach(() => {
    page = new SaiagPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
